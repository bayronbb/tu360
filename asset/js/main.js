(function() {
    scrollTo();
})();

function scrollTo() {
    const links = document.querySelectorAll('.anclas-boton');
    links.forEach(each => (each.onclick = scrollAnchors));
}

function scrollAnchors(e, respond = null) {
    const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);
    e.preventDefault();
    var targetID = (respond) ? respond.getAttribute('data-target') : this.getAttribute('data-target');
    const targetAnchor = document.querySelector(targetID);
    if (!targetAnchor) return;
    const originalTop = distanceToTop(targetAnchor);
    window.scrollBy({ top: originalTop, left: 0, behavior: 'smooth' });
    const checkIfDone = setInterval(function() {
        const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
        if (distanceToTop(targetAnchor) === 0 || atBottom) {
            targetAnchor.tabIndex = '-1';
            targetAnchor.focus();
            /* window.history.pushState('', '', targetID); */
            clearInterval(checkIfDone);
        }
    }, 100);
}

document.querySelector('#personas-tab').addEventListener('click', function(e){
    if(e.target.classList.contains('active')){      
    }else{
        document.querySelector('#subtitle').classList.remove('hidden');
        document.querySelector('#subtitle1').classList.add('hidden');
    }
});

document.querySelector('#negocios-tab').addEventListener('click', function(f){
    if(f.target.classList.contains('active')){      
    }else{
        document.querySelector('#subtitle').classList.add('hidden');
        document.querySelector('#subtitle1').classList.remove('hidden');
        
    }
});
function selector(elemento){
    let tabContent = document.querySelector(elemento);
    tabContent.classList.add('show');
    tabContent.classList.add('active');
}

selector("#movilidad #nav-personas-1");
selector("#movilidad #nav-tab a");

selector("#inmobiliario #nav-personas-inmobiliario-1");
selector("#inmobiliario #nav-tab a");
